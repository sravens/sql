SELECT
    e.display_name                                 AS 'Virtual Machine'
  , REPLACE(cl.group_name, 'VMs_', '')             AS 'Cluster Name'
  , IF(c.change       IS NULL, '', c.change)       AS 'vCPU change'
  , IF(c.first        IS NULL, '', c.first)        AS 'vCPU first'
  , IF(c.last         IS NULL, '', c.last)         AS 'vCPU last'
--   , IF(c.action_count IS NULL, '', c.action_count) AS 'vCPU actions'
  , IF(stats.vCPU_avg IS NULL OR c.change IS NULL, '', stats.vCPU_avg) AS 'vCPU avg(%)'
  , IF(stats.vCPU_max IS NULL OR c.change IS NULL, '', stats.vCPU_max) AS 'vCPU max(%)'
  , IF(m.change       IS NULL, '', m.change)       AS 'vMEM change'
  , IF(m.first        IS NULL, '', m.first)        AS 'vMEM first'
  , IF(m.last         IS NULL, '', m.last)         AS 'vMEM last'
--   , IF(m.action_count IS NULL, '', m.action_count) AS 'vMEM actions'
  , IF(stats.vMEM_avg IS NULL OR m.change IS NULL, '', stats.vMEM_avg) AS 'vMEM avg(%)'
  , IF(stats.vMEM_max IS NULL OR m.change IS NULL, '', stats.vMEM_max) AS 'vMEM max(%)'
  , IF(s.change       IS NULL, '', s.change)       AS 'vSTRG change'
  , IF(s.first        IS NULL, '', s.first)        AS 'vSTRG first'
  , IF(s.last         IS NULL, '', s.last)         AS 'vSTRG last'
--   , IF(s.action_count IS NULL, '', s.action_count) AS 'vSTRG actions'
  , IF(stats.vSTRG_avg IS NULL OR s.change IS NULL, '', stats.vSTRG_avg) AS 'vSTRG avg(%)'
  , IF(stats.vSTRG_max IS NULL OR s.change IS NULL, '', stats.vSTRG_max) AS 'vSTRG max(%)'
FROM
    entities e
    JOIN
        (
            SELECT DISTINCT target_object_uuid AS uuid
            FROM
                actions
            WHERE
                action_type      = 16
                AND action_state = 6 ) ae
    ON
        ae.uuid = e.uuid
    JOIN
        (
            SELECT  `egmh`.`group_uuid` AS `group_uuid`
              ,`egmh`.`internal_name`           AS `internal_name`
              ,`egmh`.`group_name`              AS `group_name`
              ,`egmh`.`group_type`              AS `group_type`
              ,`entities`.`uuid`                AS `member_uuid`
              ,`entities`.`display_name`        AS `display_name`
            FROM
                (
                    SELECT
                        `entity_assns_members_entities`.`entity_dest_id` AS `entity_dest_id`
                      ,`entity_group_assns`.`group_uuid`                 AS `group_uuid`
                      ,`entity_group_assns`.`internal_name`              AS `internal_name`
                      ,`entity_group_assns`.`group_name`                 AS `group_name`
                      ,`entity_group_assns`.`group_type`                 AS `group_type`
                    FROM
                        (`entity_group_assns`
                        LEFT JOIN
                            `entity_assns_members_entities`
                        ON
                            (
                                (
                                    `entity_group_assns`.`assn_id` = `entity_assns_members_entities`.`entity_assn_src_id`
                                )
                            )
                        )
                    ) AS `egmh`
                JOIN
                    `entities`
                ON
                    (
                        (
                            `entities`.`id` = `egmh`.`entity_dest_id`
                        )
                    )
            WHERE
                (
                    `entities`.`uuid` IS NOT NULL
                )
            ) AS egm
    ON
        egm.member_uuid = e.uuid
    LEFT JOIN
        (
            SELECT DISTINCT `egmh`.`group_uuid` AS `group_uuid`
              ,`egmh`.`internal_name`           AS `internal_name`
              ,`egmh`.`group_name`              AS `group_name`
              ,`egmh`.`group_type`              AS `group_type`
              ,`entities`.`uuid`                AS `member_uuid`
              ,`entities`.`display_name`        AS `display_name`
            FROM
                (
                    SELECT
                        `entity_assns_members_entities`.`entity_dest_id` AS `entity_dest_id`
                      ,`entity_group_assns`.`group_uuid`                 AS `group_uuid`
                      ,`entity_group_assns`.`internal_name`              AS `internal_name`
                      ,`entity_group_assns`.`group_name`                 AS `group_name`
                      ,`entity_group_assns`.`group_type`                 AS `group_type`
                    FROM
                        (`entity_group_assns`
                        LEFT JOIN
                            `entity_assns_members_entities`
                        ON
                            (
                                (
                                    `entity_group_assns`.`assn_id` = `entity_assns_members_entities`.`entity_assn_src_id`
                                )
                            )
                        )
                    ) AS `egmh`
                JOIN
                    `entities`
                ON
                    (
                        (
                            `entities`.`id` = `egmh`.`entity_dest_id`
                        )
                    )
            WHERE
                (
                    `entities`.`uuid` IS NOT NULL
                )
            ) AS cl
    ON
        cl.member_uuid = e.uuid
    LEFT JOIN
        (
            SELECT
                target_object_uuid                                                                           AS uuid
              , COUNT(1)                                                                                     AS action_count -- How many times this action has been logged
              , IF(MIN(create_time) IS NULL, DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY), MIN(create_time)) AS FIRST        -- The first occurrence or 7 DAYs ago whichever is oldest
              , IF(MAX(clear_time)  IS NULL, current_TIMESTAMP, MAX(clear_time))                             AS last         -- The last occurrence or current time if not cleared
              , details
              , IF(MAX(new) = current, NULL, CONCAT(IF(MAX(new)-current < 0,'Decrease','Increase'), ' vCPU capacity from ', ROUND(current,0), ' to ', ROUND(MAX(new),0))) AS 'change'
            FROM
                actions a
                JOIN
                    action_types at
                ON
                    at.id = a.action_type
            WHERE
                at.id              = 16
                AND a.action_state = 6
                AND (details LIKE 'Increase VCPU capacity%' OR details LIKE 'Reduce VCPU capacity%')
                AND
                (
                    create_time    > DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY)
                    OR update_time > DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY)
                )
            GROUP BY
                target_object_uuid, details ) c
    ON
        c.uuid = e.uuid
    LEFT JOIN
        (
            SELECT
                target_object_uuid                                                                           AS uuid
              , COUNT(1)                                                                                     AS action_count -- How many times this action has been logged
              , IF(MIN(create_time) IS NULL, DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY), MIN(create_time)) AS FIRST        -- The first occurrence or 7 DAYs ago whichever is oldest
              , IF(MAX(clear_time)  IS NULL, current_TIMESTAMP, MAX(clear_time))                             AS last         -- The last occurrence or current time if not cleared
              , details
              , IF(MAX(new) = current, NULL, CONCAT(IF(MAX(new)-current < 0,'Decrease','Increase'), ' vMEM capacity from ', ROUND(current/1024,0), ' to ', ROUND(MAX(new)/1024,0), ' MB')) AS 'change'
              , current
              , MAX(new)
            FROM
                actions a
                JOIN
                    action_types at
                ON
                    at.id = a.action_type
            WHERE
                at.id              = 16
                AND a.action_state = 6
                AND ( details LIKE 'Increase VMEM capacity%' OR details LIKE 'Reduce VMEM capacity%')
                AND
                (
                    create_time    > DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY)
                    OR update_time > DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY)
                )
            GROUP BY
                target_object_uuid, details ) m
    ON
        m.uuid = e.uuid
    LEFT JOIN
        (
            SELECT
                vs.uuid
              , vs.details
              , GROUP_CONCAT(vs.change SEPARATOR '; ') AS 'change'
              , MIN(vs.first)                          AS FIRST        -- use the oldest vStorage action across all vStorages
              , MAX(vs.last)                           AS last         -- use the most recent vStorage action across all vStorages
              , MAX(vs.action_count)                   AS action_count -- use the highest action count for a given vStorage
            FROM
                (
                    SELECT
                        target_object_uuid                                                                           AS uuid
                      , COUNT(1)                                                                                     AS action_count -- How many times this action has been logged
                      , IF(MIN(create_time) IS NULL, DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY), MIN(create_time)) AS FIRST        -- The first occurrence or 7 DAYs ago whichever is oldest
                      , IF(MAX(clear_time)  IS NULL, current_TIMESTAMP, MAX(clear_time))                             AS last         -- The last occurrence or current time if not cleared
                      , details
                      , IF(MAX(new) = current, NULL, CONCAT(IF(MAX(new)-current < 0,'Decrease','Increase'), ' vStorage capacity', REPLACE(Substring_index(Substring_index(details, '"',-4), '"', 1), '"', ''), ' from ', ROUND(current/1024,0), ' to ', ROUND(MAX(new)/1024,0), ' GB')) AS 'change'
                      , current
                      , MAX(new)
                    FROM
                        actions a
                        JOIN
                            action_types at
                        ON
                            at.id = a.action_type
                    WHERE
                        at.id              = 16
                        AND a.action_state = 6
                        AND (details LIKE 'Increase VStorage capacity%' OR details LIKE 'Reduce VStorage capacity%')
                        AND
                        (
                            create_time    > DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY)
                            OR update_time > DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY)
                        )
                    GROUP BY
                        target_object_uuid, details ) vs
            GROUP BY
                uuid ) s
    ON
        s.uuid = e.uuid
    LEFT JOIN
    (select uuid
,ROUND(AVG(IF(property_type = 'VMem', avg_value, NULL))*100,0) AS vMEM_avg
,ROUND(AVG(IF(property_type = 'VMem', max_value, NULL))*100,0) AS vMEM_max
,ROUND(AVG(IF(property_type = 'VCPU', avg_value, NULL))*100,0) AS vCPU_avg
,ROUND(AVG(IF(property_type = 'VCPU', max_value, NULL))*100,0) AS vCPU_max
,ROUND(AVG(IF(property_type = 'VStorage', avg_value, NULL))*100,0) AS vSTRG_avg
,ROUND(AVG(IF(property_type = 'VStorage', max_value, NULL))*100,0) AS vSTRG_max
from vm_stats_by_day
where property_type IN ('VMem', 'VCPU', 'VStorage')
and property_subtype IN ( 'utilization', 'sumUtilization' )
and commodity_key is NULL
and snapshot_time > DATE_SUB(current_TIMESTAMP, INTERVAL 7 DAY)
group by uuid) stats
ON e.uuid = stats.uuid

WHERE
    e.creation_class        = 'VirtualMachine'
--    AND egm.internal_name   = 'GROUP-USER-GroupName'
    AND cl.internal_name LIKE 'GROUP-VMsByCluster_%'
    AND NOT
    (
        c.details     IS NULL
        AND m.details IS NULL
        AND s.details IS NULL
    )
GROUP BY e.display_name, cl.group_name
ORDER BY cl.group_name, e.display_name ASC