SELECT
    e.display_name                                 AS 'Virtual Machine'
  , REPLACE(egm.group_name, 'VMs_', '')            AS 'Group Name'
  , IF(stats.vCPU_avg IS NULL, '', stats.vCPU_avg) AS 'vCPU avg(%)'
  , IF(stats.vCPU_max IS NULL, '', stats.vCPU_max) AS 'vCPU max(%)'
  , IF(stats.vCPU_avg_used IS NULL, '', stats.vCPU_avg_used) AS 'vCPU avg(MHz)'
  , IF(stats.vCPU_max_used IS NULL, '', stats.vCPU_max_used) AS 'vCPU max(MHz)'
  , IF(stats.vCPU_cap IS NULL, '', stats.vCPU_cap) AS 'vCPU cap(MHz)'
  , IF(stats.vMEM_avg IS NULL, '', stats.vMEM_avg) AS 'vMEM avg(%)'
  , IF(stats.vMEM_max IS NULL, '', stats.vMEM_max) AS 'vMEM max(%)'
  , IF(stats.vMEM_avg_used IS NULL, '', stats.vMEM_avg_used) AS 'vMEM avg(MB)'
  , IF(stats.vMEM_max_used IS NULL, '', stats.vMEM_max_used) AS 'vMEM max(MB)'
  , IF(stats.vMEM_cap IS NULL, '', stats.vMEM_cap/1024) AS 'vMEM cap(MB)'
  , IF(stats.vSTRG_avg IS NULL, '', stats.vSTRG_avg) AS 'vSTRG avg(%)'
  , IF(stats.vSTRG_max IS NULL, '', stats.vSTRG_max) AS 'vSTRG max(%)'
  , IF(stats.vSTRG_avg_used IS NULL, '', stats.vSTRG_avg_used) AS 'vSTRG avg(GB)'
  , IF(stats.vSTRG_max_used IS NULL, '', stats.vSTRG_max_used) AS 'vSTRG max(GB)'
  , IF(stats.vSTRG_cap IS NULL, '', stats.vSTRG_cap) AS 'vSTRG cap(GB)'
FROM
    entities e
RIGHT JOIN
        (
            SELECT  `egmh`.`group_uuid` AS `group_uuid`
              ,`egmh`.`internal_name`           AS `internal_name`
              ,`egmh`.`group_name`              AS `group_name`
              ,`egmh`.`group_type`              AS `group_type`
              ,`entities`.`uuid`                AS `member_uuid`
              ,`entities`.`display_name`        AS `display_name`
            FROM
                (
                    SELECT
                        `entity_assns_members_entities`.`entity_dest_id` AS `entity_dest_id`
                      ,`entity_group_assns`.`group_uuid`                 AS `group_uuid`
                      ,`entity_group_assns`.`internal_name`              AS `internal_name`
                      ,`entity_group_assns`.`group_name`                 AS `group_name`
                      ,`entity_group_assns`.`group_type`                 AS `group_type`
                    FROM
                        (`entity_group_assns`
                        LEFT JOIN
                            `entity_assns_members_entities`
                        ON
                            (
                                (
                                    `entity_group_assns`.`assn_id` = `entity_assns_members_entities`.`entity_assn_src_id`
                                )
                            )
                        )
                    ) AS `egmh`
                JOIN
                    `entities`
                ON
                    (
                        (
                            `entities`.`id` = `egmh`.`entity_dest_id`
                        )
                    )
            WHERE
                (
                    `entities`.`uuid` IS NOT NULL
                )
            ) AS egm
    ON
        egm.member_uuid = e.uuid    
    LEFT JOIN
    (select uuid
,ROUND(AVG(IF(property_type = 'VMem' AND property_subtype = 'utilization', avg_value, NULL))*100,0) AS vMEM_avg
,ROUND(AVG(IF(property_type = 'VMem' AND property_subtype = 'utilization', max_value, NULL))*100,0) AS vMEM_max
,ROUND(AVG(IF(property_type = 'VMem' AND property_subtype = 'used', avg_value/1024, NULL)),0) AS vMEM_avg_used
,ROUND(AVG(IF(property_type = 'VMem' AND property_subtype = 'used', max_value/1024, NULL)),0) AS vMEM_max_used
,ROUND(AVG(IF(property_type = 'VMem' AND property_subtype = 'used', capacity/1024, NULL)),0) AS vMEM_cap
,ROUND(AVG(IF(property_type = 'VCPU' AND property_subtype = 'utilization', avg_value, NULL))*100,0) AS vCPU_avg
,ROUND(AVG(IF(property_type = 'VCPU' AND property_subtype = 'utilization', max_value, NULL))*100,0) AS vCPU_max
,ROUND(AVG(IF(property_type = 'VCPU' AND property_subtype = 'used', avg_value, NULL)),0) AS vCPU_avg_used
,ROUND(AVG(IF(property_type = 'VCPU' AND property_subtype = 'used', max_value, NULL)),0) AS vCPU_max_used
,ROUND(AVG(IF(property_type = 'VCPU' AND property_subtype = 'used', capacity, NULL)),0) AS vCPU_cap
,ROUND(AVG(IF(property_type = 'VStorage' AND property_subtype = 'sumUtilization', avg_value, NULL))*100,0) AS vSTRG_avg
,ROUND(AVG(IF(property_type = 'VStorage' AND property_subtype = 'sumUtilization', max_value, NULL))*100,0) AS vSTRG_max
,ROUND(AVG(IF(property_type = 'VStorage' AND property_subtype = 'sumUsed', avg_value/1024, NULL)),0) AS vSTRG_avg_used
,ROUND(AVG(IF(property_type = 'VStorage' AND property_subtype = 'sumUsed', max_value/1024, NULL)),0) AS vSTRG_max_used
,ROUND(AVG(IF(property_type = 'VStorage' AND property_subtype = 'sumUsed', capacity/1024, NULL)),0) AS vSTRG_cap

from vm_stats_by_hour
where property_type IN ('VMem', 'VCPU', 'VStorage')
and property_subtype IN ( 'utilization', 'sumUtilization', 'used', 'sumUsed' )
-- and commodity_key is NULL
and snapshot_time > DATE_SUB(current_TIMESTAMP, INTERVAL 2 DAY)
and HOUR(snapshot_time) >= 8 
and HOUR(snapshot_time) <= 18
group by uuid) stats
ON e.uuid = stats.uuid

WHERE
    e.creation_class        = 'VirtualMachine'
    AND egm.internal_name LIKE 'GROUP-VMsByCluster_%'       -- Filter out only VMs by Cluster group
    AND egm.group_name LIKE 'VMs_%\\Prod%'                  -- Filter out only VMs by Cluster group
    AND stats.vCPU_avg IS NOT NULL
ORDER BY stats.vCPU_avg DESC
LIMIT 20